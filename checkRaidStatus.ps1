# Copyright (c) 2015 All Right Reserved, https://noshutdown.ru/
# author: 	noshutdown.ru
# email:	info@noshutdown.ru
# date:		14-11-2015
# #############################################################

[Reflection.Assembly]::LoadWithPartialName("Microsoft.Storage.Vds") | Out-Null

$VdsServiceLoader = New-Object Microsoft.Storage.Vds.ServiceLoader 
$VdsService = $VdsServiceLoader.LoadService($null) 
$VdsService.WaitForServiceReady() 

$volumes = $VdsService.Providers  | foreach {$_.packs} | foreach {$_.volumes}
$RAID_ERROR = 0

function sendMail {
	$EMAIL_USER="admin@demo.com"
	$EMAIL_PASSWORD="password"
	$EMAIL_TO="support@demo.com"
	$EMAIL_SUBJECT="RAID is degraded"
	$EMAIL_BODY="WARNING: RAID is degraded, call to support: +7-351-755-52-51"
	$EMAIL_SMTP_SERVER="smtp.yandex.ru"

	$SMTPClient = New-Object Net.Mail.SmtpClient($EMAIL_SMTP_SERVER, 587) 
	$SMTPClient.EnableSsl = $true 
	$SMTPClient.Credentials = New-Object System.Net.NetworkCredential($EMAIL_USER,$EMAIL_PASSWORD); 
	$SMTPClient.Send($EMAIL_USER, $EMAIL_TO, $EMAIL_SUBJECT, $EMAIL_BODY)
}

$volumes | foreach {
	IF ($_.Health -ne "Healthy") { $RAID_ERROR = 1 }
}

IF ($RAID_ERROR -eq 1) { sendMail }