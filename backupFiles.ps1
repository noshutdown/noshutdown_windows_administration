# Copyright (c) 2015-2016 All Right Reserved, https://noshutdown.ru/
# author:   noshutdown.ru
# email:    info@noshutdown.ru
# date:     14-11-2015
# #############################################################
  
#System Variable for backup Procedure 
 
 $date = Get-Date -Format HH.mm-d.MM.yyyy  
 $source = "C:\files" 
 $destination = "C:\backups" 
 $path = test-Path $destination 
  
# Backup to local directory 
Add-Type -assembly "system.io.compression.filesystem"
[io.compression.zipfile]::CreateFromDirectory($source, $destination+"/database-"+$date+".zip") 

# Backup to remote ftp server
$File = "C:\Backups\"+"database-"+$date+".zip";
$ftp = "ftp://username:password@192.168.0.1/"+"database-"+$date+".zip"
$webclient = New-Object -TypeName System.Net.WebClient;
$uri = New-Object -TypeName System.Uri -ArgumentList $ftp;
$webclient.UploadFile($uri, $File);
